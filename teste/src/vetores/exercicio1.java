package vetores;

import java.util.Scanner;

public class exercicio1 {

	private int X[] = new int[10];
		
	Scanner tecla = new Scanner(System.in);
	
	
	// METODOS
	
	public void ocupaVetor() {
		
		for(int i=0; i<10; i++) {
								
			X[i] = 30;		
		}		
	}
	
	public void escreveVetor() {
		
		System.out.print("[ " );
		
		for (int k =0 ; k<10; k++) {
			
			if(k<9) 
				System.out.print( X[k] + " , " );
			
			else 
				System.out.print( X[k] + " ]" );			
		}
				
	}
	
	
	// CONSTRUTORES
	
	public exercicio1() {
		
	}
	
	
	
	
	public static void main(String[] args) {
		
								
		exercicio1 gatilho = new exercicio1(); 
		
		gatilho.ocupaVetor();
		
		gatilho.escreveVetor();
						
	
	}

}


