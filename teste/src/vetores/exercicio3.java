package vetores;

import java.util.Scanner;

public class exercicio3 {

	private int B[] = new int[10];
		
	Scanner tecla = new Scanner(System.in);
	
	
	// METODOS
	
	public void ocupaVetor() {
		
		for(int i=0; i<10; i++) {
								
			if (i%2 == 0) 
							B[i] = 0;		
			else
							B[i] = 1;
		}		
	}
	
	public void escreveVetor() {
		
		System.out.print("[ " );
		
		for (int k =0 ; k<10; k++) {
			
			if(k<9) 
				System.out.print( B[k] + " , " );
			
			else 
				System.out.print( B[k] + " ]" );			
		}
				
	}
	
	
	// CONSTRUTORES
	
	public exercicio3() {
		
	}
	
	
	
	
	public static void main(String[] args) {
		
								
		exercicio3 gatilho = new exercicio3(); 
		
		gatilho.ocupaVetor();
		
		gatilho.escreveVetor();
						
	
	}

}


