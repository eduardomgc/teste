package vetores;

import java.util.Scanner;

public class exercicio2 {

	private int A[] = new int[10];
		
	Scanner tecla = new Scanner(System.in);
	
	
	// METODOS
	
	public void ocupaVetor() {
		
		for(int i=1; i<=10; i++) {
								
			A[i-1] = i;		
		}		
	}
	
	public void escreveVetor() {
		
		System.out.print("[ " );
		
		for (int k =0 ; k<10; k++) {
			
			if(k<9) 
				System.out.print( A[k] + " , " );
			
			else 
				System.out.print( A[k] + " ]" );			
		}
				
	}
	
	
	// CONSTRUTORES
	
	public exercicio2() {
		
	}
	
	
	
	
	public static void main(String[] args) {
		
								
		exercicio2 gatilho = new exercicio2(); 
		
		gatilho.ocupaVetor();
		
		gatilho.escreveVetor();
						
	
	}

}


